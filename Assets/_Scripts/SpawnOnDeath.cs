﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpawnOnDeath : MonoBehaviour
{

    public GameObject prefab;

    private void OnDestroy()
    {
        Instantiate(prefab);
    }
}
