﻿using System;
using System.Collections;
using System.Collections.Generic;
using _Scripts;
using UnityEngine;
using UnityEngine.Serialization;

public class EnemyDeploy : MonoBehaviour
{

    private BoundedMovement _movement;

    public LayerMask deployTrigger;

    private void Awake()
    {
        // _movement = gameObject.transform.parent.GetComponent<BoundedMovement>();
        _movement = gameObject.transform.GetComponent<BoundedMovement>();
    }

    private void OnTriggerEnter2D(Collider2D other)
    {
        if (Utils.isLayerInMask(other.gameObject.layer, deployTrigger))
        {
            try
            {
                Transform p = gameObject.transform.parent;
                p.parent = null;
                transform.parent = null;
                p.SetParent(transform);
                if (PlayerValues.PlayerX < transform.position.x)
                {
                    _movement.ReverseDirection();
                }

                _movement.enabled = true;

            }
            catch
            {
                // ignored
            }
        }
    }
}
