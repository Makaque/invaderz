﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GlobalValueManager : MonoBehaviour
{

    private static GlobalValueManager _instance = null;

    public static GlobalValueManager Instance => _instance;

    public float FrameStepTime;

    private void Awake()
    {
        if (_instance == null)
        {
            _instance = this;
        }
        else if(_instance != this)
        {
            Destroy(gameObject);
        }
        
        DontDestroyOnLoad(gameObject);
    }

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
