﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerMovement : MonoBehaviour
{

    private bool _left;
    private bool _right;
    private bool _shoot;
    private IShooter _shooter;

    private void Awake()
    {
        _shooter = GetComponentInChildren<IShooter>();
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.LeftArrow))
        {
            _left = true;
        }

        if (Input.GetKeyDown(KeyCode.RightArrow))
        {
            _right = true;
        }

        if (Input.GetKeyDown(KeyCode.Space))
        {
            _shoot = true;
        }
    }

    private void FixedUpdate()
    {
        if (_left)
        {
            transform.position += (1.5f * Vector3.left);
        }

        if (_right)
        {
            transform.position += (1.5f * Vector3.right);
        }

        if (_shoot)
        {
            _shooter.Shoot();
        }

        transform.position = new Vector2(Mathf.Clamp(transform.position.x, -10.5f, 10.5f), transform.position.y);

        _left = false;
        _right = false;
        _shoot = false;
    }
}
