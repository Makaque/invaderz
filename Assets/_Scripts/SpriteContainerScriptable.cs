﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "SpriteContainer", menuName = "Containers/SpriteContainer")]
public class SpriteContainerScriptable : ScriptableObject
{
    public Sprite sprite;
    public float xBoundingBoxSize = 1;
    public float yBoundingBoxSize = 1;
    public float xBoundingBoxOffset = 0;
    public float yBoundingBoxOffset = 0;
}
