﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Score : MonoBehaviour
{

    public static int score;

    public static void AddPoints(int value)
    {
        score += value;
    }

    public void ResetScore()
    {
        score = 0;
    }

    private void Awake()
    {
        ResetScore();
    }
}
