﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RestartSpawner : MonoBehaviour
{
    public float delay = 5;

    public GameObject restartMenuPrefab;

    // Update is called once per frame
    void Update()
    {
        delay -= Time.deltaTime;
        if (delay < 0)
        {
            Instantiate(restartMenuPrefab);
            Destroy(gameObject);
        }
    }
}