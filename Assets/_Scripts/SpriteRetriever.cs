﻿using System;
using System.Collections;
using System.Collections.Generic;
using _Scripts;
using UnityEngine;

public class SpriteRetriever : MonoBehaviour
{
    // Start is called before the first frame update
    void Awake()
    {
        SpriteValue spriteValue = GetComponentInParent<SpriteValue>();
        SpriteRenderer spriteRenderer = GetComponentInChildren<SpriteRenderer>();
        BoxCollider2D[] colliders = GetComponentsInChildren<BoxCollider2D>();
        spriteRenderer.sprite = spriteValue.spriteValue.sprite;

        foreach (var col in colliders)
        {
            var spriteTransform = col.transform;
            spriteTransform.localPosition = new Vector2(spriteValue.spriteValue.xBoundingBoxOffset,
                spriteValue.spriteValue.yBoundingBoxOffset);
            spriteTransform.localScale = new Vector2(spriteValue.spriteValue.xBoundingBoxSize,
                spriteValue.spriteValue.yBoundingBoxSize);
        }
    }
}