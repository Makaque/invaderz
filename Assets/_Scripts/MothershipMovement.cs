﻿using System;
using System.Collections;
using System.Collections.Generic;
using _Scripts;
using UnityEngine;
using Random = UnityEngine.Random;

public class MothershipMovement : BoundedMovement
{
    public float lateralDistance = 0.3f;
    public float shotMinTime = 5;
    public float shotMaxTime = 20;
    private IShooter shooter;
    private bool _shoot;
    private bool _pause;
    private float _nextShot;

    protected override float LateralDistance => lateralDistance;


    private void Awake()
    {
        shooter = GetComponent<IShooter>();
        _nextShot = NextShot();
    }

    // Start is called before the first frame update
    void Start()
    {
    }

    // Update is called once per frame
    void Update()
    {
        _nextShot -= Time.deltaTime;
        if (_nextShot < 0)
        {
            _shoot = true;
        }

        // if (_nextShot < 1)
        // {
        //     _pause = true;
        // }
    }

    private void FixedUpdate()
    {
        if (_shoot)
        {
            StartCoroutine(nameof(Shoot));
            _shoot = false;
            _nextShot = NextShot();

        }
        else
        {
            if (!_pause)
            {
                LateralMove();
            }
        }

        if (BoundaryHit)
        {
            ReverseDirection();
            BoundaryHit = false;
        }
    }

    private float NextShot()
    {
        return Random.Range(shotMinTime, shotMaxTime);
    }

    // private void Shoot()
    // {
    //     shooter.Shoot();
    //     _shoot = false;
    // }

    IEnumerator Shoot()
    {
        _pause = true;
        yield return new WaitForSeconds(1.5f);
        shooter.Shoot();
        yield return new WaitForSeconds(1.5f);
        _pause = false;
    }
}