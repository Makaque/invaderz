﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RegisterEnemy : MonoBehaviour
{
    private void Awake()
    {
        RespawnBlock.Register(gameObject);
    }

    private void OnDestroy()
    {
        RespawnBlock.Unregister(gameObject);
    }
}
