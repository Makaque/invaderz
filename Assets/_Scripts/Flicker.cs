﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Flicker : MonoBehaviour
{

    public float timeout = 0.01f;
    private float _last;

    private void Awake()
    {
        _last = timeout;
    }

    // Update is called once per frame
    void Update()
    {
        _last -= Time.deltaTime;
    }

    private void FixedUpdate()
    {
        if (_last < 0)
        {
            var localScale = transform.localScale;
            localScale = new Vector3(-localScale.x, localScale.y, localScale.z);
            transform.localScale = localScale;
            _last = timeout;
        }
    }
}
