﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ShotEmitter : MonoBehaviour, IShooter
{

    public GameObject ShotPrefab;
    

    public void Shoot()
    {
        Object.Instantiate(ShotPrefab).transform.position = transform.position;
    }
}
