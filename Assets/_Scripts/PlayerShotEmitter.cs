﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerShotEmitter : MonoBehaviour, IShooter
{
    public IShooter Left;
    public IShooter Right;
    public float shotCooldown;
    private bool _toggle = true;
    private float _last;

    private void Awake()
    {
        IShooter[] shooters = GetComponentsInChildren<IShooter>();
        Left = shooters[1];
        Right = shooters[2];
        _last = shotCooldown;
    }

    private void Update()
    {
        _last -= Time.deltaTime;
    }

    public void Shoot()
    {
        if (_last < 0)
        {
            if (_toggle)
            {
                Left.Shoot();
            }
            else
            {
                Right.Shoot();
            }

            _toggle = !_toggle;
            _last = shotCooldown;
        }
    }
}