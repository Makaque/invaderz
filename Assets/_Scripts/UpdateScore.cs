﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UpdateScore : MonoBehaviour
{
    // public Score score;

    public int pointValue = 1;

    private void OnDestroy()
    {
        Score.AddPoints(pointValue);
    }
}
