﻿using System;
using System.Collections;
using System.Collections.Generic;
using _Scripts;
using UnityEngine;

public abstract class BoundedMovement : MonoBehaviour
{
    public LayerMask boundaryLayerMask;


    private int _direction = 1;

    public int Direction => _direction;

    private bool _boundaryHit = false;

    public bool BoundaryHit
    {
        get => _boundaryHit;
        set => _boundaryHit = value;
    }

    protected abstract float LateralDistance { get; }


    public void ReverseDirection()
    {
        _direction *= -1;
    }

    protected void LateralMove()
    {
        transform.position += Direction * LateralDistance * Vector3.right;
    }

    protected void OnTriggerEnter2D(Collider2D other)
    {
        if (Utils.isLayerInMask(other.gameObject.layer, boundaryLayerMask))
        {
            _boundaryHit = true;
        }
    }
}