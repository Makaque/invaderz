﻿using UnityEngine;

namespace _Scripts
{
    public class Utils
    {
        public static bool isLayerInMask(int layer, LayerMask mask)
        {
            return mask == (mask | (1 << layer));
        }
    }
}