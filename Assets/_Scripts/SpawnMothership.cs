﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpawnMothership : MonoBehaviour
{
    public float delay = 20;
    private float _cooldown;

    public GameObject mothershipPrefab;

    private void Awake()
    {
        ResetCooldown();
    }

    private void ResetCooldown()
    {
        _cooldown = Time.time + delay;
    }

    private void CooldownTick()
    {
        _cooldown -= Time.deltaTime;
    }

    private bool IsCooldownUp()
    {
        return Time.time > _cooldown;
    }

    private void Spawn()
    {
        GameObject mothership = Instantiate(mothershipPrefab, gameObject.transform, false);
    }

    private void Update()
    {
        CooldownTick();
        if (IsCooldownUp())
        {
            Spawn();
            ResetCooldown();
        }
    }
}