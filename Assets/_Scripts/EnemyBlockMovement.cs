﻿using System;
using System.Collections;
using System.Collections.Generic;
using _Scripts;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.Serialization;
using UnityEngine.UI;

public class EnemyBlockMovement : BoundedMovement
{
    public float wait = 0.5f;
    public float percentSpeedIncrease = 5;
    public float dropDistance;
    public float lateralDistance;
    public LayerMask hitLayerMask;

    private float _last;

    protected override float LateralDistance => lateralDistance;

    // private int _direction = 1;
    // private bool _boundaryHit = false;

    // Start is called before the first frame update
    void Awake()
    {
        _last = wait;
    }

    private void Update()
    {
        if (Math.Abs(transform.position.x) > 40 || Math.Abs(transform.position.y) > 40)
        {
            Destroy(gameObject);
        }
    }

    private void FixedUpdate()
    {
        _last -= Time.deltaTime;
        if (_last < 0)
        {
            Move();
            _last = wait;
        }
    }

    private void Move()
    {
        if (BoundaryHit)
        {
            DropDown();
            ReverseDirection();
            BoundaryHit = false;
        }
        else
        {
            LateralMove();
        }
    }




    private void DropDown()
    {
        transform.position += dropDistance * Vector3.down;
    }



    private new void OnTriggerEnter2D(Collider2D other)
    {
        base.OnTriggerEnter2D(other);
        if (Utils.isLayerInMask(other.gameObject.layer, hitLayerMask))
        {
            IncreaseSpeed();
        }
    }

    private void IncreaseSpeed()
    {
        // wait *= (1 - (percentSpeedIncrease / 100));
        wait -= wait * percentSpeedIncrease * 0.01f;
    }
}