﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyDeployedMovement : BoundedMovement
{
    public float wait;
    [SerializeField] private float lateralDistance;
    
    protected override float LateralDistance => lateralDistance;

    private float _last;

    private void Awake()
    {
        _last = wait;
    }

    private void FixedUpdate()
    {
        _last -= Time.deltaTime;
        if (_last < 0)
        {
            Move();
            _last = wait;
        }
    }


    private void Move()
    {
        if (BoundaryHit)
        {
            ReverseDirection();
            BoundaryHit = false;
        }

        LateralMove();
    }

}