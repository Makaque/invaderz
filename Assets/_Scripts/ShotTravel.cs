﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Serialization;

public class ShotTravel : MonoBehaviour
{

    public float timeout = 0.35f;
    public int direction = 1;
    public float distance = 0.5f;
    private float _last;
    private Vector3 _direction;
    

    private void Awake()
    {
        _last = timeout;
        _direction = (direction == 1) ? Vector3.up : Vector3.down;
    }

    private void FixedUpdate()
    {
        _last -= Time.deltaTime;
        if (_last < 0)
        {
            Move();
            _last = timeout;
        }
    }

    private void Move()
    {
        transform.position += (distance * _direction);
    }
}
