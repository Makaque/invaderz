﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SoundOnDeath : MonoBehaviour
{
    public GameObject DeathSoundPrefab;
    private void OnDestroy()
    {
        Instantiate(DeathSoundPrefab);
    }
}
