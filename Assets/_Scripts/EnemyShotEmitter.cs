﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SocialPlatforms;
using Random = UnityEngine.Random;

public class EnemyShotEmitter : MonoBehaviour, IShooter
{
    private IShooter _gun;
    public float wait = 3;



    public float variance = 0.5f;
    [Range(0,100)]
    public int chance = 20;

    private bool _enabled = true;
    private bool _shoot = false;
    private float _last;

    public float Wait => Random.Range(wait + variance, wait - variance);

    // Update is called once per frame
    void Update()
    {
        _last -= Time.deltaTime;
        if (_last < 0)
        {
            _shoot = Roll();
        }
    }

    private void FixedUpdate()
    {
        if (_shoot)
        {
            Shoot();
        }
    }
    
    

    private void Awake()
    {
        _gun = GetComponent<ShotEmitter>();
        _last = Wait;
    }

    private void OnTriggerEnter2D(Collider2D other)
    {
        Disable();
    }


    private void OnTriggerExit2D(Collider2D other)
    {
        Enable();
    }

    private void Enable()
    {
        _enabled = true;
    }

    private void Disable()
    {
        _enabled = false;
    }

    public void Shoot()
    {
        if (_enabled)
        {
            _gun.Shoot();
        }
        _shoot = false;
        _last = Wait;
    }

    private bool Roll()
    {
        _last = Wait;
        return Random.Range(0, 100) <= chance;
    }
}