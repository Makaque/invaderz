﻿using System;
using System.Collections;
using System.Collections.Generic;
using _Scripts;
using UnityEngine;
using UnityEngine.Serialization;

public class Fragile : MonoBehaviour
{
    private Self _self;
    private bool _armed = false;

    public LayerMask breakOn;

    private void Awake()
    {
        _self = GetComponent<Self>();
    }

    private void OnCollisionEnter2D(Collision2D other)
    {
        if (Utils.isLayerInMask(other.gameObject.layer, breakOn))
        {
            DestroySelf();
        }
    }

    private void OnTriggerEnter2D(Collider2D other)
    {
        if (Utils.isLayerInMask(other.gameObject.layer, breakOn))
        {
            DestroySelf();
        }
    }


    private void OnTriggerExit2D(Collider2D other)
    {
        Arm();
    }

    private void Arm()
    {
        _armed = true;
    }

    private void DestroySelf()
    {
        if (_armed)
        {
            Destroy(_self.self.gameObject);
        }
    }
}