﻿using System.Collections;
using System.Collections.Generic;
using _Scripts;
using UnityEngine;

public class BoundaryReversible : MonoBehaviour
{
    
    public LayerMask boundaryLayerMask;
    public int _direction = 1;
    
    private bool _boundaryHit = false;

    public int Direction => _direction;

    public bool BoundaryHit => _boundaryHit;


    private void ReverseDirection()
    {
        _direction *= -1;
    }
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }
    
    
    private void OnTriggerEnter2D(Collider2D other)
    {
        if (Utils.isLayerInMask(other.gameObject.layer, boundaryLayerMask))
        {
            _boundaryHit = true;
        }
    }
}
