﻿using System;
using System.Collections;
using System.Collections.Generic;
using _Scripts;
using UnityEngine;

public class Destroyer : MonoBehaviour
{
    public LayerMask DestroyLayer;
    public LayerMask MutualDestroyLayer;
    public int HP = 100;
    private Self _self;

    private void Awake()
    {
        _self = GetComponent<Self>();
    }

    private void OnTriggerEnter2D(Collider2D other)
    {
        DestroyOther(other);
    }

    private void DestroyOther(Collider2D other)
    {
        if (Utils.isLayerInMask(other.gameObject.layer, MutualDestroyLayer))
        {
            var otherRoot = other.GetComponent<Self>();
            Destroy(otherRoot.self);
            Destroy(_self.self);
        }

        if (Utils.isLayerInMask(other.gameObject.layer, DestroyLayer))
        {
            var otherRoot = other.GetComponent<Self>();
            Destroy(otherRoot.self);
            TakeHit();
        }
    }

    private void TakeHit()
    {
        HP--;
        if (HP < 1)
        {
            Destroy(_self.self);
        }
    }
}
