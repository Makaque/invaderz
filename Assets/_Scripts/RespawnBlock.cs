﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RespawnBlock : MonoBehaviour
{
    public GameObject EnemyBlockPrefab;
    private static int enemiesRemaining;
    private static bool _respawning;


    public static void Register(GameObject enemy)
    {
        enemiesRemaining += 1;
    }

    public static void Unregister(GameObject enemy)
    {
        enemiesRemaining -= 1;
    }

    // Update is called once per frame
    void Update()
    {
        if (enemiesRemaining == 0 && !_respawning)
        {
            _respawning = true;
            StartCoroutine("Respawn");
        }
    }

    IEnumerator Respawn()
    {
        yield return new WaitForSeconds(2);
        foreach (var componentsInChild in GetComponentsInChildren<Transform>())
        {
            if (componentsInChild.gameObject != gameObject)
            {
                Destroy(componentsInChild.gameObject);
            }
        }
        GameObject obj = Instantiate(EnemyBlockPrefab);
        obj.transform.parent = gameObject.transform;
        
        _respawning = false;
    }
}
